{

;============================================================
;============================================================
; Solare
;============================================================
;============================================================
Name       	"Solare"


;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime   	true
Selectable 	true
;)CPUSelectable	true
;)Statistics 	true
Class      	0 			; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain     	0 			; Obtain method
Rating     	1 			; Skill level (rookie, amateur, ...)
TopEnd     	3037.062988 		; Actual top speed (mph) for frontend bars
Acc        	8.551951 		; Acceleration rating (empirical)
Weight     	1.500000 		; Scaled weight (for frontend bars)
Trans      	0 			; Transmission type (0=4WD, 1=FWD, 2=RWD)
MaxRevs    	0.500000 		; Max Revs (for rev counter, deprecated...)

;====================
; Model Filenames
;====================

MODEL 	0 	"cars/fd74solar/body.prm"
MODEL 	1 	"cars/fd74solar/wheelfl.prm"
MODEL 	2 	"cars/fd74solar/wheelfr.prm"
MODEL 	3 	"cars/fd74solar/wheelbl.prm"
MODEL 	4 	"cars/fd74solar/wheelbr.prm"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"NONE"
MODEL 	9 	"cars/fd74solar/axle.prm"
MODEL 	10 	"NONE"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars/misc/Aerial.m"
MODEL 	18 	"cars/misc/AerialT.m"
COLL 		"cars/fd74solar/hull.hul"
TPAGE 		"cars/fd74solar/car.bmp"
;)TCARBOX  	"cars/fd74solar/carbox.bmp" 			; Carbox texture
;)TSHADOW 	"cars/fd74solar/shadow.bmp"
;)SHADOWINDEX 	-1 			; Use a default shadow (0 to 27 or -1)
;)SHADOWTABLE 	-47.700001 47.700001 84.199997 -76.400002 -3.500000 	; Left, right, front, back, height (relative to model center)
;)SFXENGINE 	"cars/fd74solar/moto.wav"
EnvRGB 		255 255 255

;====================
; Handling related stuff
;====================

SteerRate  	3.000000 			; Rate at which steer angle approaches value from input
SteerMod   	0.400000 			;
EngineRate 	4.500000 			; Rate at which Engine voltage approaches set value
TopSpeed   	37.199999 			; Car's theoretical top speed (not including friction...)
DownForceMod	2.000000 			; Down force modifier when car on floor
CoM        	0.000000 -4.000000 -3.000000 	; Centre of mass relative to model centre
Weapon     	0.000000 -32.000000 64.000000 	; Weapon generation offset
;)Flippable	false 		; Rotor car effect
;)Flying   	false 		; Flying like the UFO car
;)ClothFx  	false 		; Mystery car cloth effect

;====================
; Camera details
;====================

;)CAMATTACHED {	; Start Camera
;)HoodOffset   	0.000000 0.000000 0.000000 	; Offset from model center
;)HoodLook     	0.050000 			; Look angle (-0.25 to 0.25, 0.0 - straight ahead)
;)RearOffset   	0.000000 0.000000 0.000000
;)RearLook     	0.050000
;)UseDefault   	true 				; Use default offsets (computed in game)
;)}            	; End Camera

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			; Model Number in above list
Offset     	0.000000 0.000000 0.000000
Mass       	1.500000
Inertia    	1030.000000 0.000000 0.000000
           	0.000000 1230.000000 0.000000
           	0.000000 0.000000 260.000000
Gravity    	2200 			; No longer used
Hardness   	0.000000
Resistance 	0.001000 		; Linear air resistance
AngRes     	0.001000 		; Angular air resistance
ResMod     	25.000000 		; AngRes scale when in air
Grip       	0.010000 		; Converts downforce to friction value
StaticFriction 	0.800000
KineticFriction 0.400000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-12.000000 3.000000 37.000000
Offset2  	-9.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	true
SteerRatio  	-0.400000
EngineRatio 	5000.000000
Radius      	10.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	3.000000
SkidWidth   	11.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    0.020000
Grip            0.020000
StaticFriction  1.800000
KineticFriction 1.750000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	12.000000 3.000000 37.000000
Offset2  	9.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	true
SteerRatio  	-0.400000
EngineRatio 	5000.000000
Radius      	10.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	3.000000
SkidWidth   	11.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    0.020000
Grip            0.020000
StaticFriction  1.800000
KineticFriction 1.750000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	3
Offset1  	-17.000000 -3.000000 -41.000000
Offset2  	-9.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	false
SteerRatio  	0.100000
EngineRatio 	15000.000000
Radius      	18.000000
Mass        	0.400000
Gravity     	2200.000000
MaxPos      	5.000000
SkidWidth   	20.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    0.050000
Grip            0.020000
StaticFriction  1.900000
KineticFriction 1.850000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	4
Offset1  	17.000000 -3.000000 -41.000000
Offset2  	9.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	false
SteerRatio  	0.100000
EngineRatio 	15000.000000
Radius      	18.000000
Mass        	0.400000
Gravity     	2200.000000
MaxPos      	5.000000
SkidWidth   	20.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    0.050000
Grip            0.020000
StaticFriction  1.900000
KineticFriction 1.850000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	-1
Offset      	-13.000000 -15.000000 32.000000
Length      	6.700000
Stiffness   	700.000000
Damping     	10.000000
Restitution 	-0.950000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	-1
Offset      	13.000000 -15.000000 32.000000
Length      	6.700000
Stiffness   	700.000000
Damping     	10.000000
Restitution 	-0.950000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	-1
Offset      	-12.000000 -28.000000 -44.500000
Length      	6.700000
Stiffness   	700.000000
Damping     	10.000000
Restitution 	-0.950000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	-1
Offset      	12.000000 -28.000000 -44.500000
Length      	6.700000
Stiffness   	700.000000
Damping     	10.000000
Restitution 	-0.950000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	-1
Offset      	-7.000000 -2.000000 35.000000
Length      	12.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	-1
Offset      	7.000000 -2.000000 35.000000
Length      	12.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	9
Offset      	-7.000000 -4.000000 -41.000000
Length      	12.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	9
Offset      	7.000000 -4.000000 -41.000000
Length      	12.000000
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 1.000000 0.000000
AngVel      	0.000000
;)Type      	1 				; 1: Default rot, 2: Turn with steer, 4: Translate with speed, 6: 2 and 4
;)Trans     	0.000000 3.000000 6.000000 	; Translation max
;)TransVel  	0.001000 			; Velocity factor
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	0.000000 -18.000000 -10.000000
Direction   	-0.000000 -0.900000 -0.000000
Length      	20.000000
Stiffness   	2000.000000
Damping     	5.500000
}           	; End Aerial


;====================
; Car AI details
;====================

AI {        	; Start AI
UnderThresh 	644.596680
UnderRange  	1500.000000
UnderFront  	724.559998
UnderRear   	335.000000
UnderMax    	0.950000
OverThresh  	1010.361633
OverRange   	1391.000000
OverMax     	0.545923
OverAccThresh  	10.000000
OverAccRange   	569.190002
PickupBias     	29490
BlockBias      	16383
OvertakeBias   	3276
Suspension     	29490
Aggression     	0
}           	; End AI

}

