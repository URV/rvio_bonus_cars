{

;============================================================
;============================================================
; Orbit Runner
;============================================================
;============================================================
Name      	"Orbit Runner"


;====================
; Model Filenames
;====================

MODEL 	0 	"cars\gtc_orbitrunner\body.prm"
MODEL 	1 	"cars\gtc_orbitrunner\wheelleft.prm"
MODEL 	2 	"cars\gtc_orbitrunner\wheelright.prm"
MODEL 	3 	"NONE"
MODEL 	4 	"NONE"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"NONE"
MODEL 	9 	"NONE"
MODEL 	10 	"NONE"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars\misc\Aerial.m"
MODEL 	18 	"cars\misc\AerialT.m"
TPAGE 	"cars\gtc_orbitrunner\car.bmp"
TCARBOX  "cars\gtc_orbitrunner\carbox.bmp"
COLL 	"cars\gtc_orbitrunner\hull.hul"
SFXENGINE 	"cars\gtc_orbitrunner\engine2.wav"
TSHADOW 	"cars\gtc_orbitrunner\shadow.bmp"
SHADOWINDEX 	-1
SHADOWTABLE -76.4252 76.4252 82.9137 -73.9137 -0.3049
EnvRGB 	100 100 100

;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime   	TRUE
Selectable 	TRUE
CPUSelectable	TRUE
Statistics 	TRUE
Class      	1 			; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain     	0 			; Obtain method
Rating     	3 			; Skill level (rookie, amateur, ...)
TopEnd     	3128.168701 			; Actual top speed (mph) for frontend bars
Acc        	5.786904 			; Acceleration rating (empirical)
Weight     	1.500000 			; Scaled weight (for frontend bars)
Handling   	50.000000 			; Handling ability (empirical and totally subjective)
Trans      	2 			; Transmission type (calculate in game anyway...)
MaxRevs    	0.500000 			; Max Revs (for rev counter)

;====================
; Handling related stuff
;====================

SteerRate  	2.500000 			; Rate at which steer angle approaches value from input
SteerMod   	0.400000 			;
EngineRate 	2.10000 			; Rate at which Engine voltage approaches set value
TopSpeed   	34.600000 			; Car's theoretical top speed (not including friction...)
DownForceMod	4.000000 			; Down force modifier when car on floor
CoM        	0.000000 0.700000 3.000000 		; Centre of mass relative to model centre
Weapon     	0.000000 -32.000000 64.000000 		; Weapon genration offset

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			; Model Number in above list
Offset     	0, 0, 0 		; Calculated in game
Mass       	1.500000
Inertia    	1230.000000 0.000000 0.000000
           	0.000000 1825.000000 0.000000
           	0.000000 0.000000 1200.000000
Gravity		2200 			; No longer used
Hardness   	0.00000
Resistance 	0.00105 		; Linear air resistance
AngRes     	0.00130 		; Angular air resistance
ResMod     	25.000000 		; AngRes scale when in air
Grip       	0.01000 		; Converts downforce to friction value
StaticFriction 	0.800000
KineticFriction 0.400000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-22.000000 -4.300000 38.500000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	FALSE
IsTurnable  	TRUE
SteerRatio  	-0.260000
EngineRatio 	20000.000000
Radius      	10.500000
Mass        	0.130000
Gravity     	2200.000000
MaxPos      	3.000000
SkidWidth   	8.000000
ToeIn       	0.000000
AxleFriction    	0.040000
Grip            	0.025000
StaticFriction  	2.140000
KineticFriction 	2.120000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	22.000000 -4.300000 38.500000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	FALSE
IsTurnable  	TRUE
SteerRatio  	-0.260000
EngineRatio 	20000.000000
Radius      	10.500000
Mass        	0.130000
Gravity     	2200.000000
MaxPos      	3.000000
SkidWidth   	8.000000
ToeIn       	0.000000
AxleFriction    	0.040000
Grip            	0.025000
StaticFriction  	2.140000
KineticFriction 	2.120000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	1
Offset1  	-22.000000 -4.300000 -32.000000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	1.000000
EngineRatio 	24000.000000
Radius      	10.500000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	3.000000
SkidWidth   	8.000000
ToeIn       	0.000000
AxleFriction    	0.040000
Grip            	0.023000
StaticFriction  	2.130000
KineticFriction 	2.100000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	2
Offset1  	22.000000 -4.300000 -32.000000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	1.000000
EngineRatio 	24000.000000
Radius      	10.500000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	3.000000
SkidWidth   	8.000000
ToeIn       	0.000000
AxleFriction    	0.040000
Grip            	0.023000
StaticFriction  	2.130000
KineticFriction 	2.100000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	950.000000
Damping     	14.000000
Restitution 	-0.800000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	950.000000
Damping     	14.000000
Restitution 	-0.800000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	950.000000
Damping     	14.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	950.000000
Damping     	14.000000
Restitution 	-0.850000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 1.000000 0.000000
AngVel      	0.000000
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	0.000000 -27.000000 -33.000000
Direction   	0.000000 -1.000000 0.000000
Length      	13.000000
Stiffness   	2000.000000
Damping     	5.500000
}           	; End Aerial


;====================
; Car AI details
;====================

AI {        	 ;Start AI
UnderThresh 	381.824083
UnderRange  	1192.638841
UnderFront	1123.504187
UnderRear   	421.885344
UnderMax    	0.395497
OverThresh  	2521.723389
OverRange   	1276.065365
OverMax     	0.488563
OverAccThresh  	43.428896
OverAccRange   	2113.864502
PickupBias     	3276
BlockBias      	3276
OvertakeBias   	16383
Suspension     	0
Aggression     	0
}           	; End AI

}

C1FC6B7E