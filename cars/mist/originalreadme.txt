=======================================================================
ORM Family Sedan

Initial release:	1-28-2002

Ingame Name:		Family Sedan

Install directory:	\cars\CL1

Author:			Ordmet

Base:			New Mesh/Textures from scratch;
			Original Hull:		Humma
			Original Params:	Adeon

Editors used:		3D Studio MAX 4.2, Photoshop 6.0, ase2prm,
			hulscale

Known Bugs:		Couldn't get the .hul to fit quite right due to
			the mesh's unusual size. The fore and aft as
			well as the top are pretty good, but the bottom
			is a bit soft. It's not particularly noticeable
			unless you fall slowly off of a curb, so it's
			not too big of a deal.

Description:

 This is the car your grandma drives.
 This is the car that holds your groceries (in the 80's).
 This is not the car that comes to mind when you think about the car
 that "you'd like to own someday."
 This is not the car you wash lovingly by hand on late Sunday
 afternoons (or ever).

 This car follows no styling rules (and breaks most of them).

 This is the Family Sedan.

 Yet another dependable car from the makers of the Woodmobile*. 
(* shamelessly licensed from Chrysler. However, the Family Sedan is an
all-original design. Honest!).

Vital Stats:

 Curb weight: 			1.5 (lbs?)
 Length (bumper to bumper): 	0.145ft
 Width: 			0.074ft
 Height (body only): 		0.037ft
 Faces (body only):		532
 Verticies (body only):		268
 Doors: 			4
 Spoiler:			Optional
 Cargo space: 			more than most SUVs these days
 Top speed (observed):		34mph/315fpm
 Rating:			Semi-Pro
 Powerplant:			2 stroke glow engine
 Recommended fuel:		20% unleaded nitro
 Miles between maintenance:	0.43 (estimated)
 EPA Mileage City/Highway:	TBA*

*EPA refused to test this model, citing rising fuel costs.
Ordmet Motors demands (and will eventually win in court) satisfaction,
however.

Thanks to:

 Acclaim for making it all possible.
 Acclaim Studios London for making it all possible.
 Wayne Lemonds for keeping us all together.
 The community for staying together.
 Ali for ase2prm.
 srmalloy for hulscale.
 The Me and Me
 Bubba Z
 ����rT�r�
 RiffRaff
 Rex R

Legal:

 Authors MAY use this car as a base to build additional cars. Note that
 Ordmet Motors does not endorse this permit. (i.e. don't associate
 derivative works with ORM)

 You MAY distribute this CAR, provided you include this file, with
 no modifications.  You may distribute this file in any electronic
 format (BBS, Diskette, CD, etc) as long as you include this file 
 intact.
=======================================================================