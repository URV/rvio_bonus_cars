=====================================================================
=====================================================================
=====================================================================
=====================================================================
Details:

Name:                                Zaffer
Authors:                               Kipy
Category:                          Original
Class:                              Amateur    
Engine type:                       Electric
Trans:                                  4WD
Top Speed:	          33.5 mph/53.9 kph
Folder's name:       .../ReVolt/cars/lib14c/

=====================================================================
=====================================================================
=====================================================================
=====================================================================
=====================================================================

This car based on the sketch which can be found in gallery folder.
The picture named lib14c, which illustrates car selection view with
a early prototype of Dust Mite, at least it is close to that.
Scratch-made car, which has average appearance in Amateur class.
Handling is smooth (even with battery too) sometimes slightly
slippery.

=====================================================================
=====================================================================
Credits:

Copyright belongs to Kipy who worked on the car.
You can repaint/modify, but ALWAYS mention the authors of the car.

=====================================================================
=====================================================================
							2018.08.27.
=====================================================================
=====================================================================