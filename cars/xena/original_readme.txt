Car information
================================================================
Car name                : Wheelie
Car Type  		: Original
Top speed 		: 46 mph/kph
Rating/Class   		: 3 (semi-pro)
Installed folder       	: ...\cars\Wheelie
Description             : A vibrant monster truck made from my desire to make a car with bouncy suspension. 
			: Got the idea from playing with Mouse.
			: 
 
Author Information
================================================================
Author Name 		: BBLIR
 
Construction
================================================================
Editor(s) used 		: Blender, Photoshop, MS Paint

Copyright / Permissions
================================================================
Nothing special, just have fun and don't claim it as your own.
Repainting is allowed.
 