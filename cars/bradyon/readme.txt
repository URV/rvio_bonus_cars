Car Information
================================================================
Car name : Bradyon
Car Type : Repaint/Reparam/Remap/Rehull/Reshade
Top Speed : 41mph / 67kph
Rating/Class : 4 (pro)
Install folder : ...\cars\bradyon
Description: Modification of Aeon's Tachyon.

Only thing left over from the original is the baseline mesh, everything else (mapping, vertex shading, shadow, hull) has been re-made. Wheels are taken off of Zipper and also remapped.

Handling is somewhat similar to RG1, though a bit heavier. It turns rather slowly, requiring you to either lay on the brakes or lose the rear every so often.
It's wide and flat, so once its flipped it's not getting back up until you flip it back yourself.


Author Information
================================================================
Author Name : Raster Gotolei
Email Address : rgotolei@gmail.com


Construction
================================================================
Base : Tachyon
Editors used : Blender for mapping changes, hull and help with painting, Inkscape for rims, Gimp for skin/box, Gedit for text-related things


Additional Credits
================================================================
Aeon, for the car this is based off of
Jigebren, for blender plugin and prm2hul
Iconian Fonts, for Radio Space


Copyright / Permissions
================================================================
You may do whatever you want with this car, noting the reqs set in readme-aeon.txt.
