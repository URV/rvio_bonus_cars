================================================================
TACHYON
Created by Aeon (archakelon@gmail.com)
Class 4 (Pro)
Place in: /cars/Tachyon
================================================================
Custom mesh created by Aeon.
Wheels modified from RiffRaff's SHIVAN car.
Axles borrowed from RiffRaff's PURPILL car.
Textures by Yves Allaire, borrowed from Quake.

2820 polygons, including wheels and axles.
================================================================
Thanks for RiffRaff for his other POD conversions and the
wheels that he converted and which I am borrowing and modifying,
and thanks to Yves Allaire for creating the original textures.
================================================================
You may use this car however you like and distribute it anywhere,
but please give credit where its due.