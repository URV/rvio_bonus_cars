{

;============================================================
;============================================================
; Mugello
;============================================================
;============================================================
Name      	"Mugello"


;====================
; Models
;====================

MODEL 	0 	"cars\jg8mugello\body.prm"
MODEL 	1 	"cars\jg8mugello\wheel-l.prm"
MODEL 	2 	"cars\jg8mugello\wheel-r.prm"
MODEL 	3 	"NONE"
MODEL 	4 	"NONE"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"NONE"
MODEL 	9 	"NONE"
MODEL 	10 	"NONE"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars\misc\Aerial.m"
MODEL 	18 	"cars\misc\AerialT.m"
TPAGE 	"cars\jg8mugello\car.bmp"
;)TCARBOX "cars\jg8mugello\carbox.bmp"
;)TSHADOW "cars\jg8mugello\shadow.bmp"
;)SHADOWTABLE -86.2836 86.2836 89.4836 -83.4836 -20.2595
COLL 	"cars\jg8mugello\hull.hul"
EnvRGB 	100 100 100

;====================
; Frontend
;====================

BestTime   	TRUE
Selectable 	TRUE
;)Statistics	TRUE
Class      	1
Obtain     	0
Rating     	4
TopEnd     	3834.401367
Acc        	7.028000
Weight     	1.725000
Handling   	50.000000
Trans      	2
MaxRevs    	0.500000

;====================
; Handling
;====================

SteerRate  	3.000000
SteerMod   	0.000000
EngineRate 	4.500000
TopSpeed   	42.000000
DownForceMod	2.000000
CoM        	0.000000 -12.000000 0.000000
Weapon     	0.000000 -32.000000 64.000000

;====================
; Body
;====================

BODY {		; Start Body
ModelNum   	0
Offset     	0, 0, 0
Mass       	1.725000
Inertia    	1325.000000 0.000000 0.000000
           	0.000000 1870.000000 0.000000
           	0.000000 0.000000 680.000000
Gravity		2200
Hardness   	0.000000
Resistance 	0.001000
AngRes     	0.001000
ResMod     	25.000000
Grip       	0.010000
StaticFriction  0.800000
KineticFriction 0.400000
}     		; End Body

;====================
; Wheels
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-26.000000 9.000000 36.250000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	FALSE
IsTurnable  	TRUE
SteerRatio  	-0.250000
EngineRatio 	0.000000
Radius      	10.750000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	4.000000
SkidWidth   	13.000000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.020000
StaticFriction  	1.920000
KineticFriction 	1.860000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	26.000000 9.000000 36.250000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	FALSE
IsTurnable  	TRUE
SteerRatio  	-0.250000
EngineRatio 	0.000000
Radius      	10.750000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	4.000000
SkidWidth   	13.000000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.020000
StaticFriction  	1.920000
KineticFriction 	1.860000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	1
Offset1  	-26.500000 9.250000 -38.500000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	0.000000
EngineRatio 	28000.000000
Radius      	10.750000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	4.000000
SkidWidth   	13.000000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.022000
StaticFriction  	2.000000
KineticFriction 	1.940000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	2
Offset1  	26.500000 9.250000 -38.500000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	0.000000
EngineRatio 	28000.000000
Radius      	10.750000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	4.000000
SkidWidth   	13.000000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.022000
StaticFriction  	2.000000
KineticFriction 	1.940000
}          	; End Wheel


;====================
; Springs
;====================

SPRING 0 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	1000.000000
Damping     	15.000000
Restitution 	-0.750000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	1000.000000
Damping     	15.000000
Restitution 	-0.750000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	900.000000
Damping     	15.000000
Restitution 	-0.750000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	900.000000
Damping     	15.000000
Restitution 	-0.750000
}           	; End Spring


;====================
; Pins
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Axles
;====================

AXLE 0 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle


;====================
; Spinner
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 0.000000 0.000000
AngVel      	0.000000
}           	; End Spinner


;====================
; Aerial
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	-20.000000 -6.500000 -45.000000
Direction   	0.000000 -1.000000 0.000000
Length      	14.000000
Stiffness   	2000.000000
Damping     	5.500000
}           	; End Aerial


;====================
; AI
;====================

AI {        	 ;Start AI
UnderThresh	196.190002
UnderRange	1660.855469
UnderFront	2810.185791
UnderRear	734.534729
UnderMax	0.950000
OverThresh	1247.171387
OverRange	2196.582031
OverMax		0.790000
OverAccThresh	368.045593
OverAccRange	494.211060
PickupBias	16383
BlockBias	16383
OvertakeBias	16383
Suspension	9830
Aggression	0
}           	; End AI