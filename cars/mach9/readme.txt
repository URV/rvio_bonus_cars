-- MACH 9 ---------

-- CAR INFO -------------

Name: Mach 9
Creator: Hi-Ban
Car Type: Original
Engine Class: Electric
Rating: Pro
Top Speed: 38 mph (61km/h)

-- FEATURES -------------

- Custom Body mesh.
- Custom Spring mesh.
- Custom Wheel meshes.
- Custom Axle mesh.
- Custom Textures.
- Custom Hull.
- Custom Parameters.
- Custom Car Box.
- Custom Shadow.

-- NOTES ----------------

This is a custom car based on a concept drawing done by "Killer Wheels" from the ORP forum.

The parameters have been tweaked to be balanced with the stock Pro cars.

You can freely make a repaint, or use parts of this car (springs, wheels...) as long as you credit me accordingly in your readme.

-- CONTACT --------------

hi-ban@hotmail.com
