{

;============================================================
;============================================================
; Dasher
;============================================================
;============================================================
Name       	"Pebble Dasher"


;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime   	true
Selectable 	true
;)CPUSelectable	true
;)Statistics 	true
Class      	0 			; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain     	0 			; Obtain method
Rating     	4 			; Skill level (rookie, amateur, ...)
TopEnd     	3492.762695 		; Actual top speed (mph) for frontend bars
Acc        	4.118515 		; Acceleration rating (empirical)
Weight     	2.1000000		; Scaled weight (for frontend bars)
Trans      	0 			; Transmission type (0=4WD, 1=FWD, 2=RWD)
MaxRevs    	0.500000 		; Max Revs (for rev counter, deprecated...)

;====================
; Model Filenames
;====================

MODEL 	0 	"cars/nm_dasher/body.prm"
MODEL 	1 	"cars/nm_dasher/wheelfl.prm"
MODEL 	2 	"cars/nm_dasher/wheelfr.prm"
MODEL 	3 	"cars/nm_dasher/wheelbl.prm"
MODEL 	4 	"cars/nm_dasher/wheelbr.prm"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"NONE"
MODEL 	9 	"cars/nm_dasher/axle.prm"
MODEL 	10 	"cars/nm_dasher/spring.prm"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"none"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars/misc/Aerial.m"
MODEL 	18 	"cars/misc/AerialT.m"
COLL 		"cars/nm_dasher/hull.hul"
TPAGE 		"cars/nm_dasher/car.bmp"
;)TCARBOX 	"cars/nm_dasher/Carbox.bmp" 			; Carbox texture
;)TSHADOW 	"cars/nm_dasher/shadow.bmp" 			; Shadow texture
;)SHADOWINDEX 	-1 							; Use a default shadow (0 to 27 or -1)
;)SHADOWTABLE 	-65.000000 65.0000000 65.000000 -61.199997 -12.700000 	; Left, right, front, back, height (relative to model center)
;)SFXENGINE 	"NONE"
;)SFXSERVO 	"NONE"
;)SFXHONK 	"NONE"
EnvRGB 		50 50 50

;====================
; Handling related stuff
;====================

SteerRate  	8.000000 			; Rate at which steer angle approaches value from input
SteerMod   	0.400000 			;
EngineRate 	4.500000 			; Rate at which Engine voltage approaches set value
TopSpeed   	33.000001 			; Car's theoretical top speed (not including friction...)
DownForceMod	2.000000 			; Down force modifier when car on floor
CoM        	0.000000 -5.000000 -2.000000 	; Centre of mass relative to model centre
Weapon     	0.000000 -32.000000 64.000000 	; Weapon generation offset
;)Flippable	false 		; Rotor car effect
;)Flying   	false 		; Flying like the UFO car
;)ClothFx  	false 		; Mystery car cloth effect

;====================
; Camera details
;====================

;)CAMATTACHED {	; Start Camera
;)HoodOffset   	0.000000 0.000000 0.000000 	; Offset from model center
;)HoodLook     	0.050000 			; Look angle (-0.25 to 0.25, 0.0 - straight ahead)
;)RearOffset   	0.000000 0.000000 0.000000
;)RearLook     	0.050000
;)FixedOffset  	true 				; Is offset fixed or moving
;)FixedLook    	true 				; Is look fixed or moving
;)UseDefault   	true 				; Use default offsets (computed in game)
;)}            	; End Camera

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			; Model Number in above list
Offset     	0.000000 0.000000 0.000000
Mass       	2.100000
Inertia    	2000.000000 0.000000 0.000000
           	0.000000 3000.000000 0.000000
           	0.000000 0.000000 1100.000000
Gravity    	2200 			; No longer used
Hardness   	0.000000
Resistance 	0.001000 		; Linear air resistance
AngRes     	0.001000 		; Angular air resistance
ResMod     	25.000000 		; AngRes scale when in air
Grip       	0.120000 		; Converts downforce to friction value
StaticFriction 	1.300000
KineticFriction 0.800000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-22.000000 5.000000 35.000000
Offset2  	-4.500000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	true
SteerRatio  	-0.300000
EngineRatio 	30000.000000
Radius      	13.000000
Mass        	0.200000
Gravity     	2200.000000
MaxPos      	15.000000
SkidWidth   	12.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    0.060000
Grip            0.015000
StaticFriction  1.750000
KineticFriction 1.700000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	22.000000 5.000000 35.000000
Offset2  	4.500000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	true
SteerRatio  	-0.300000
EngineRatio 	30000.000000
Radius      	13.000000
Mass        	0.200000
Gravity     	2200.000000
MaxPos      	15.000000
SkidWidth   	12.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    0.060000
Grip            0.015000
StaticFriction  1.750000
KineticFriction 1.700000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	3
Offset1  	-21.000000 4.000000 -40.000000
Offset2  	-6.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	true
SteerRatio  	0.010000
EngineRatio 	30000.000000
Radius      	14.000000
Mass        	0.200000
Gravity     	2200.000000
MaxPos      	15.000000
SkidWidth   	16.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    0.090000
Grip            0.035000
StaticFriction  1.800000
KineticFriction 1.800000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	4
Offset1  	21.000000 4.000000 -40.000000
Offset2  	6.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	true
SteerRatio  	0.010000
EngineRatio 	30000.000000
Radius      	14.000000
Mass        	0.200000
Gravity     	2200.000000
MaxPos      	15.000000
SkidWidth   	16.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    0.090000
Grip            0.035000
StaticFriction  1.800000
KineticFriction 1.800000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	10
Offset      	-12.000000 -14.000000 35.000000
Length      	15.000000
Stiffness   	550.000000
Damping     	15.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	10
Offset      	12.000000 -14.000000 35.000000
Length      	15.000000
Stiffness   	550.000000
Damping     	15.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	10
Offset      	-14.000000 -20.000000 -40.000000
Length      	15.000000
Stiffness   	550.000000
Damping     	15.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	10
Offset      	14.000000 -20.000000 -40.000000
Length      	15.000000
Stiffness   	550.000000
Damping     	15.000000
Restitution 	-0.850000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	9
Offset      	-8.000000 -2.000000 35.000000
Length      	12.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	9
Offset      	8.000000 -2.000000 35.000000
Length      	12.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	9
Offset      	-8.000000 -5.000000 -40.000000
Length      	12.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	9
Offset      	8.000000 -5.000000 -40.000000
Length      	12.000000
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	-15.000000 -26.000000 -30.000000
Axis        	0.000000 1.000000 0.000000
AngVel      	1.000000
;)Type      	1 				; 1: Default rot, 2: Turn with steer, 4: Translate with speed, 6: 2 and 4
;)Trans     	0.000000 3.000000 6.000000 	; Translation max
;)TransVel  	0.001000 			; Velocity factor
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	11.500000 -17.500000 -40.000000
Direction   	0.000000 -1.000000 0.000000
Length      	20.000000
Stiffness   	2000.000000
Damping     	5.500000
}           	; End Aerial


;====================
; Car AI details
;====================

AI {        	; Start AI
UnderThresh 	82.150002
UnderRange  	3162.633545
UnderFront  	251.520004
UnderRear   	1125.232666
UnderMax    	0.271073
OverThresh  	1704.070923
OverRange   	1391.000000
OverMax     	0.746230
OverAccThresh  	113.930000
OverAccRange   	2574.388916
PickupBias     	16383
BlockBias      	16383
OvertakeBias   	16383
Suspension     	9830
Aggression     	0
}           	; End AI

}

