
  M E L O N    C R U S H E R

  Version 1.0 from September 15th, 2020

================================================================
  Car information
================================================================

Date      : 15/09/2020
Car Name  : Melon Crusher
Author    : Kiwi, Skarma
Car Type  : Remodel
Folder    : ...\cars\meloncrusher
Top Speed : 60 kph
Mass      : 2,7 KG
Rating    : 3 (Advanced)

Melon Crusher is a Monster Truck based on Baja Bug by Scloink. 
My initial idea was, to create a stock-like version of Tamiyas
Monster Beetle. I got the idea for the skin after me and my
wife ate a watermelon on a hot summer day. :)

The Advanced-parameters are done by Skarma. The custom honk is
from the 1998 released UDS game "Ignition".

The download includes a paintkit. Several optional parts (front
bumper, blower, headlights, side mirrors) can be activated/
deactivated by using pureblacks at the texture file.

Have fun!
- Kiwi


================================================================
  Construction
================================================================

Polygons       : 1601 (Body, Axles, Shocks, Wheels)

Base           : Baja Bug by Scloink
                 Monster Beetle by Kiwi
                 Skull Crusher by Trixed

Editors used   : Blender 2.79b
                 Ulead Photo Impact 12
                 Audacity
                 Notepad++


================================================================
  Thank You
================================================================

+ Skarma for the parameters
+ Scloink for the base model
+ My wife Bianca for the watermelon inspiration
+ Trixed for the awesome Skull Crusher as my main inspiration
+ Elevencreative for the Turtle watermelon image
+ UDS for the honk sound


================================================================
  Copyright / Permissions
================================================================

Please do not change any parts of this car without asking, as
long it's not for your personal use. You can reuse any parts of
this car for your own car, as long you mention the original
creators accordingly.
