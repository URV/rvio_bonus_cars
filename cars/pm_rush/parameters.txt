{

;============================================================
;============================================================
; Fuel Rush
;============================================================
;============================================================
Name       	"Fuel Rush"


;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime   	true
Selectable 	true
;)CPUSelectable	true
;)Statistics 	true
Class      	1 			; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain     	0 			; Obtain method
Rating     	4 			; Skill level (rookie, amateur, ...)
TopEnd     	3545.859863 		; Actual top speed (mph) for frontend bars
Acc        	3.991380 		; Acceleration rating (empirical)
Weight     	2.000000 		; Scaled weight (for frontend bars)
Trans      	0 			; Transmission type (0=4WD, 1=FWD, 2=RWD)
MaxRevs    	0.500000 		; Max Revs (for rev counter, deprecated...)

;====================
; Model Filenames
;====================

MODEL 	0 	"cars/pm_rush/body.prm"
MODEL 	1 	"cars/pm_rush/wheelfl.prm"
MODEL 	2 	"cars/pm_rush/wheelfr.prm"
MODEL 	3 	"cars/pm_rush/wheelbl.prm"
MODEL 	4 	"cars/pm_rush/wheelbr.prm"
MODEL 	5 	"cars/pm_rush/axlell.prm"
MODEL 	6 	"cars/pm_rush/axlerr.prm"
MODEL 	7 	"cars/pm_rush/spring.prm"
MODEL 	8 	"NONE"
MODEL 	9 	"NONE"
MODEL 	10 	"NONE"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars/misc/Aerial.m"
MODEL 	18 	"cars/misc/AerialT.m"
COLL 		"cars/pm_rush/hull.hul"
TPAGE 		"cars/pm_rush/car.bmp"
;)TCARBOX 	"cars/pm_rush/carbox.bmp" 			; Carbox texture
;)TSHADOW 	"cars/pm_rush/shadow.bmp" 			; Shadow texture
;)SHADOWINDEX 	-1 							; Use a default shadow (0 to 27 or -1)
;)SHADOWTABLE 	-54.000000 54.000000 60.000000 -58.000000 -7.700000 	; Left, right, front, back, height (relative to model center)
;)SFXENGINE 	"NONE"
;)SFXSERVO 	"NONE"
;)SFXHONK 	"NONE"
EnvRGB 		200 200 200

;====================
; Handling related stuff
;====================

SteerRate  	3.000000 			; Rate at which steer angle approaches value from input
SteerMod   	2.000000 			;
EngineRate 	4.500000 			; Rate at which Engine voltage approaches set value
TopSpeed   	37.700001 			; Car's theoretical top speed (not including friction...)
DownForceMod	2.000000 			; Down force modifier when car on floor
CoM        	0.000000 -4.000000 -5.000000 	; Centre of mass relative to model centre
Weapon     	0.000000 -32.000000 64.000000 	; Weapon generation offset (deprecated)
;)WeaponOffset1	0.000000 -32.000000 64.000000 	; Offset for shockwaves and fireworks
;)WeaponOffset2	0.000000 -16.000000 80.000000 	; Offset for water bombs
;)Flippable	false 		; Rotor car effect
;)Flying   	false 		; Flying like the UFO car
;)ClothFx  	false 		; Mystery car cloth effect

;====================
; Camera details
;====================

;)CAMATTACHED {	; Start Camera
;)HoodOffset   	0.000000 0.000000 0.000000 	; Offset from model center
;)HoodLook     	0.050000 			; Look angle (-0.25 to 0.25, 0.0 - straight ahead)
;)RearOffset   	0.000000 0.000000 0.000000
;)RearLook     	0.050000
;)FixedOffset  	true 				; Is offset fixed or moving
;)FixedLook    	true 				; Is look fixed or moving
;)UseDefault   	true 				; Use default offsets (computed in game)
;)}            	; End Camera

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			; Model Number in above list
Offset     	0.000000 0.000000 0.000000
Mass       	2.000000
Inertia    	1800.000000 0.000000 0.000000
           	0.000000 2500.000000 0.000000
           	0.000000 0.000000 700.000000
Gravity    	2200 			; No longer used
Hardness   	0.001500
Resistance 	0.001000 		; Linear air resistance
AngRes     	0.001800 		; Angular air resistance
ResMod     	25.000000 		; AngRes scale when in air
Grip       	0.500000 		; Converts downforce to friction value
StaticFriction 	0.800000
KineticFriction 0.400000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-21.000000 5.000000 41.000000
Offset2  	-5.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	true
SteerRatio  	-0.300000
EngineRatio 	35000.000000
Radius      	13.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	13.000000
SkidWidth   	10.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    0.020000
Grip            0.018000
StaticFriction  1.900000
KineticFriction 1.900000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	21.000000 5.000000 41.000000
Offset2  	5.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	true
SteerRatio  	-0.300000
EngineRatio 	35000.000000
Radius      	13.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	13.000000
SkidWidth   	10.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    0.020000
Grip            0.018000
StaticFriction  1.900000
KineticFriction 1.900000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	3
Offset1  	-21.000000 5.000000 -36.000000
Offset2  	-8.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	false
SteerRatio  	0.000000
EngineRatio 	36000.000000
Radius      	13.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	13.000000
SkidWidth   	13.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    0.050000
Grip            0.020000
StaticFriction  1.900000
KineticFriction 1.900000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	4
Offset1  	21.000000 5.000000 -36.000000
Offset2  	8.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	false
SteerRatio  	0.000000
EngineRatio 	36000.000000
Radius      	13.000000
Mass        	0.200000
Gravity     	2200.000000
MaxPos      	13.000000
SkidWidth   	13.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    0.050000
Grip            0.020000
StaticFriction  1.900000
KineticFriction 1.900000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	7
Offset      	-9.000000 -2.500000 35.000000
Length      	17.000000
Stiffness   	300.000000
Damping     	10.000000
Restitution 	-0.950000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	7
Offset      	9.000000 -2.500000 35.000000
Length      	17.000000
Stiffness   	300.000000
Damping     	10.000000
Restitution 	-0.950000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	7
Offset      	-10.000000 -14.000000 -38.000000
Length      	17.000000
Stiffness   	300.000000
Damping     	10.000000
Restitution 	-0.950000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	7
Offset      	10.000000 -14.000000 -38.000000
Length      	17.000000
Stiffness   	300.000000
Damping     	10.000000
Restitution 	-0.950000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	5
Offset      	-7.000000 1.000000 41.000000
Length      	14.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	6
Offset      	7.000000 1.000000 41.000000
Length      	14.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	5
Offset      	-7.000000 1.000000 -36.000000
Length      	14.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	6
Offset      	7.000000 1.000000 -36.000000
Length      	14.000000
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 1.000000 0.000000
AngVel      	0.000000
;)Type      	1 				; 1: Default rot, 2: Turn with steer, 4: Translate with speed, 6: 2 and 4
;)Trans     	0.000000 3.000000 6.000000 	; Translation max
;)TransVel  	0.001000 			; Velocity factor
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	-13.000000 -8.000000 -26.000000
Direction   	0.000000 -1.000000 0.000000
Length      	25.000000
Stiffness   	2000.000000
Damping     	5.500000
}           	; End Aerial


;====================
; Car AI details
;====================

AI {        	; Start AI
UnderThresh 	190.000000
UnderRange  	2050.000000
UnderFront  	2800.000000
UnderRear   	700.000000
UnderMax    	0.950000
OverThresh  	3300.000000
OverRange   	3200.000000
OverMax     	0.800000
OverAccThresh  	300.000000
OverAccRange   	500.000000
PickupBias     	3276
BlockBias      	3276
OvertakeBias   	16383
Suspension     	0
Aggression     	0
}           	; End AI

}

