This is an Audi Avus Concept car built in 1991.                         7-1-00

The mesh was converted using Ali and Antimorphs Ase2Prm.exe tool.

The mesh utilizes smoothing groups and vertex color shading.

The wheels are modeled after the concept design.

The handling is (surprise) almost exactly like any other car I build. Change it if you want.

The paint is hand drawn. Boo-hiss but I didnt have any good texture sets of this car, and I wanted it to look like the original build. I grew tired of painting so if I missed anything, feel free to do it yourself, ;-)

Tools: 3dsMaxR3.1 with Ase2prm.exe for conversion. Texport3 plugin to help paint the car.
       Adobe Photoshop 4.1
       MSPaint V.?

Extra huge credit for mesh development goes to CADster. Using Lightwave and QemLoss2 he was able to bring this model from over 12,000 polies to 1499 polies. The mesh lost very little detail that couldnt be painted back on. Thanks CADster!!

There are no rules regarding this model. You can repaint, retune, redistribute, retard, redrum or whatever you want to it. If you leave credit for the model to me in the readme, Thanks. If not, no biggie.

Thanks and enjoy it.

RiffRaff