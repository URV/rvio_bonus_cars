Siktrix

Made by: Krobe

Siktrix originally started as KRBD, one of my entries for the 1st Haus All-Goes event.This car's mesh
is an original design, inspired heavily from rallycross cars. Likewise, its parameters attempt to 
emulate the behavior of such cars, hence its formidable acceleration and soft suspension.

Special thanks goes to:

-Bence Hajducsek and Zeino, for organizing such a wonderful event
-Xarc and Mightycucumber, for ther decal sheets
-The awesome people at the Re-Volt.io discord, for their support and appreciation during its development

Please do not distribute elsewhere without my permission.

Contact me on discord: krobe#1375