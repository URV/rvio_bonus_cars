{

;============================================================
;============================================================
; Devil's Bison
;============================================================
;============================================================
Name      	"Devil's Bison"


;====================
; Model Filenames
;====================

MODEL 	0 	"cars\phim_bison\body.prm"
MODEL 	1 	"cars\phim_bison\wheel-l.prm"
MODEL 	2 	"cars\phim_bison\wheel-r.prm"
MODEL 	3 	"NONE"
MODEL 	4 	"NONE"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"NONE"
MODEL 	9 	"NONE"
MODEL 	10 	"NONE"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars\misc\Aerial.m"
MODEL 	18 	"cars\misc\AerialT.m"
TPAGE 		"cars\phim_bison\car.bmp"
;)TCARBOX 	"cars\phim_bison\box.bmp"
;)TSHADOW 	"cars\phim_bison\shadow.bmp"
;)SHADOWTABLE -94.9982 94.9982 112.3960 -67.3960 0
COLL 	"cars\phim_bison\hull.hul"
EnvRGB 	200 200 200

;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime   	TRUE
Selectable 	TRUE
;)CPUSelectable TRUE
;)Statistics TRUE
Class      	1 			; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain     	0 			; Obtain method
Rating     	4 			; Skill level (rookie, amateur, ...)
TopEnd     	3540.934570 			; Actual top speed (mph) for frontend bars
Acc        	3.184322			; Acceleration rating (empirical)
Weight     	1.500000 			; Scaled weight (for frontend bars)
Handling   	50.000000 			; Handling ability (empirical and totally subjective)
Trans      	0 			; Transmission type (calculate in game anyway...)
MaxRevs    	0.500000 			; Max Revs (for rev counter)

;====================
; Handling related stuff
;====================

SteerRate  		3.000000 			; Rate at which steer angle approaches value from input
SteerMod   	0.400000 			;
EngineRate 	3.000000 			; Rate at which Engine voltage approaches set value
TopSpeed   	36.800000 			; Car's theoretical top speed (not including friction...)
DownForceMod	2.000000 			; Down force modifier when car on floor
CoM        	0.000000 0.000000 -5.000000 		; Centre of mass relative to model centre
Weapon     	0.000000 -32.000000 64.000000 		; Weapon genration offset

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			; Model Number in above list
Offset     	0, 0, 0 		; Calculated in game
Mass       	1.500000
Inertia    	1500.000000 0.000000 0.000000
           	0.000000 2250.000000 0.000000
           	0.000000 0.000000 840.000000
Gravity	2200 			; No longer used
Hardness   	0.000000
Resistance 	0.001000 			; Linear air esistance
AngRes     	0.001000 			; Angular air resistance
ResMod     	25.000000 			; Ang air resistnce scale when in air
Grip       	0.010000 			; Converts downforce to friction value
StaticFriction 0.800000
KineticFriction 0.400000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-25.700000 -3.500000 43.500000
Offset2  	0.000000 0.000000 0.000000
IsPresent   		TRUE
IsPowered   		TRUE
IsTurnable  		TRUE
SteerRatio  		-0.200000
EngineRatio 		30500.000000
Radius      		11.000000
Mass        		0.120000
Gravity     		2200.000000
MaxPos      		3.000000
SkidWidth   		12.000000
ToeIn       		0.000000
AxleFriction    	0.019500
Grip            	0.016000
StaticFriction  	1.860000
KineticFriction 	1.820000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	25.700000 -3.500000 43.500000
Offset2  	0.000000 0.000000 0.000000
IsPresent   		TRUE
IsPowered   	 	TRUE
IsTurnable  	 	TRUE
SteerRatio  		-0.200000
EngineRatio 		30500.000000
Radius      		11.000000
Mass        		0.120000
Gravity     		2200.000000
MaxPos      		3.000000
SkidWidth   		12.000000
ToeIn       		0.000000
AxleFriction    	0.019500
Grip            	0.016000
StaticFriction  	1.860000
KineticFriction 	1.820000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	1
Offset1  	-25.700000 -3.500000 -21.800000
Offset2  	0.000000 0.000000 0.000000
IsPresent   		TRUE
IsPowered   		TRUE
IsTurnable  		FALSE
SteerRatio  		0.100000
EngineRatio 		31500.000000
Radius      		11.000000
Mass        		0.120000
Gravity     		2200.000000
MaxPos      		3.000000
SkidWidth   		12.000000
ToeIn       		0.000000
AxleFriction    	0.050000
Grip            	0.017000
StaticFriction  	1.880000
KineticFriction 	1.840000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	2
Offset1  	25.700000 -3.500000 -21.800000
Offset2  	0.000000 0.000000 0.000000
IsPresent   		TRUE
IsPowered   		TRUE
IsTurnable  		FALSE
SteerRatio  		0.100000
EngineRatio 		31500.000000
Radius      		11.000000
Mass        		0.120000
Gravity     		2200.000000
MaxPos      		3.000000
SkidWidth   		12.000000
ToeIn       		0.000000
AxleFriction    	0.050000
Grip            	0.017000
StaticFriction  	1.880000
KineticFriction 	1.840000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	1000.000000
Damping     	15.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	1000.000000
Damping     	15.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	800.000000
Damping    	12.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	800.000000
Damping     	12.000000
Restitution 	-0.850000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 1.000000 0.000000
AngVel      	0.000000
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	-14.250000 -38.000000 17.890000
Direction   	0.000000 -1.000000 0.000000
Length      	22.000000
Stiffness   	2000.000000
Damping     	5.500000
}           	; End Aerial


;====================
; Car AI details
;====================

AI {        	 ;Start AI
UnderThresh 	550.000000
UnderRange  	2000.000000
UnderFront	 	3000.000000
UnderRear   	2856.000000
UnderMax    	0.680000
OverThresh  	700.000000
OverRange   	1331.000000
OverMax     	0.900000
OverAccThresh  	89.169998
OverAccRange   	400.000000
PickupBias     	3276
BlockBias      	3276
OvertakeBias   	16383
Suspension     	0
Aggression     	0
}           	; End AI

}

Blind Sider
