___________________________________________________

Veloce
___________________________________________________

Author: canyon-car, Norfair (mapping), MightyCucumer (visual fixes and some model tweaking)
Base: Lamborghini Diablo VT by Cat, original model by EA
___________________________________________________

Info:

Class: Glow
Top Speed: 39-40 mph
Handling: Good
Acceleration: Mediocre
Rating: Pro
___________________________________________________

Special Thanks:

-Phimeek, Tubers and MightyCucumber for their awesome sponsor decals
-Xarc for the light decals and some stuff
-Lukisav for finding the rims used in this car
-Everyone in the Re-Volt Discord Server for their feedback and help

___________________________________________________

Have fun!