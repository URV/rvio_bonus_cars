Car information
================================================================
Car name: Nova
Car Type: Conversion
Top speed: 77 kph
Rating/Class: 5 (Super Pro)
Installed folder: ...\cars\supernova
Description: 

Nova is a car for the new Super Pro class.

The body mesh was found by MightyCucumber at TurboSquid.

Mighty did the base params, I did the paintjobs. Parameter adjustments done by Saffron.

Nova comes in 16 different color combinations: Standard, Azure, Baby, Black, Black-White, Blue, Grey, Kiwi, Organge, Pink, Purple, Red, Silver, White, White-Black and Yellow.

Have fun!
-Kiwi

Author Information
================================================================
Conversion: MightyCucumber
Textures: Kiwi
Parameters: MightyCucumber, Saffron
 
Construction
================================================================
Base model: Unknown (TurboSquid)
Editor(s) used: Notepad++, Blender (with ReVolt-Plugin from Marv), PhotoImpact12
 
Additional Credits 
================================================================
+ Thanks to the guys at the RVIO community in Discord (#cars) for your help and suggestions!


Copyright / Permissions
================================================================
You may do whatever you want with this car, as long as you mention us and the original author in the credits.

Version 1.2 from February 12th, 2020
Version 1.1 from February 11th, 2020
Version 1.0 from December 27th, 2018