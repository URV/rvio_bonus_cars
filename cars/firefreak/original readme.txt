-- VORZA ---------

-- CAR INFO -------------

Name: Vorza
Creator: Hi-Ban
Car Type: Original
Engine Class: Electric
Rating: Pro
Top Speed: 40 mph (64km/h)

-- FEATURES -------------

- Custom Body mesh.
- Custom Spring mesh.
- Custom Wheel meshes.
- Custom Axle mesh.
- Custom Textures.
- Custom Hull.
- Custom Parameters.
- Custom Car Box.
- Custom Shadow.

-- NOTES ----------------

This is a replica of the HPI Vorza RC buggy.

The aim was to build a detailed car, while still fitting with the visual style of the stock cars.

The parameters have been tweaked to be balanced with the stock Pro cars.

You can freely make a repaint, or use parts of this car (springs, wheels...) as long as you credit me accordingly in your readme.

-- CONTACT --------------

hi-ban@hotmail.com
